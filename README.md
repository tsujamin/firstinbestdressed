# FirstInBestDressed
This is a short tool for automatically waiting for and enrolling in wattle tutorials. >100% more secure than the real wattle (we use SSL).

## Installation and Usage

```bash
git clone https://gitlab.cecs.anu.edu.au/tsujamin/firstinbestdressed
cd firstinbestdressed
pip3 -r requirements.txt
# prepend the next line with a space so it isn't saved in your sh history
python3 -m wattle.tutorial u12345678 supersecretpassword "http://wattlecourses.anu.edu.au/mod/groupselect/view.php?id=XXXXX" "Tutorial B - Wednesday 1400-1500"
```

The `tutorial_url` argument can be found by navigating to the tutorial enrolment page and copying the address from the navigation bar. The `tutorial_name` is the same as the group name on the tutorial enrolment page.
