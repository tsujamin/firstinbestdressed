__author__ = 'Benjamin George Roberts <benjamin.roberts@anu.edu.au>'

from requests.sessions import Session

WATTLE_LOGIN_URL = "https://wattlecourses.anu.edu.au/login/index.php"

class WattleSession(Session):

    def __init__(self, username, password):
        """
        Create a wattleSession and cache the session
        :param username: username to authenticate with
        :type username: str
        :param password: passwrd to authenticate with
        :type password: str
        :raises ConnectionRefusedError: if authentication fails
        """
        super(WattleSession, self).__init__()
        form_payload = {"username": username, "password": password}
        response = self.post(WATTLE_LOGIN_URL, data=form_payload)

        if not response.status_code == 200:
            raise ConnectionRefusedError("Invalid login details")

