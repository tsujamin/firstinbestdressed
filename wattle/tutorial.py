__author__ = 'Benjamin George Roberts <benjamin.roberts@anu.edu.au>'

from bs4 import BeautifulSoup
from time import sleep
from click import command, argument, option
from wattle.session import WattleSession

GROUP_SELECT_URL = "https://wattlecourses.anu.edu.au/mod/groupselect/view.php"

class TutorialListener():

    def __init__(self, session, tutorial_url):
        """
        Initialise a listener object for a specific tutorial
        :param session: logged in wattle session to poll with
        :type session: Session
        :param tutorial_url: url to poll on
        :type tutorial_url: str
        """
        self._session = session
        self._url = tutorial_url

    def enrol(self, timeslot, interval=1, timeout=0):
        """
        Poll the tutorial page until a timeslot becomes available
        :param timeslot: name of timeslot to enrol in
        :type timeout: str
        :param interval: time between attempts (seconds)
        :type interval: int
        :param timeout: time to poll for (seconds, 0 means forever)
        :type timeslot: int
        :return: student was enrolled
        :rtype: bool
        """
        count = 0
        while count < timeout or timeout == 0:
            if self._try_enrol(timeslot):
                print("enrolled")
                return True
            print("failed to enrol, sleeping for {0} second(s)".format(interval))
            sleep(interval)
            count += interval

        print("failed to enrol after {0} second(s)".format(timeout))
        return False

    def _try_enrol(self, timeslot):
        """
        Try and enrol the account in the specified timeslot.
        :param timeslot: name of timeslot
        :type timeslot: str
        :return: student is enrolled
        :rtype: bool
        """

        if self.is_enroled(timeslot):
            return True

        timeslot_cells = self._get_timeslot_rows()
        if timeslot not in timeslot_cells.keys():
            return False

        # Get the post params
        timeslot_form = timeslot_cells[timeslot].find("form")
        if timeslot_form is None:
            return False

        post_params = {input_cell["name"]: input_cell["value"]
                       for input_cell in timeslot_form.find_all("input")
                       if input_cell["type"] != "submit"}

        #Send enrol request, check for "submit-button" element as success requirement
        enrol_response = self._session.post(GROUP_SELECT_URL, data=post_params)
        enrol_soup = BeautifulSoup(enrol_response.text, "html.parser")
        if enrol_response.status_code != 200 or enrol_soup.find("input", id="id_submitbutton") is None:
            return False

        #build enrol post_params and post
        post_params = {input_cell["name"]: input_cell["value"]
                       for input_cell in enrol_soup.find("div", "box generalbox").find_all("input")
                       if input_cell["name"] != "cancel"}

        tutorial_soup = self._session.post(GROUP_SELECT_URL, data=post_params)

        return self.is_enroled(timeslot)

    def is_enroled(self, timeslot):
        """
        Check if the logged in account is enroled in the timeslot
        :param timeslot: name of timeslot
        :type timeslot: str
        :return: account is enroled in timeslot
        :rtype: bool
        """
        timeslots = self._get_timeslot_rows()

        if timeslot not in timeslots.keys():
            return False

        submit_button = timeslots[timeslot].find("input", type="submit")

        if submit_button is None:
            return False

        # Check if submit button starts with "leave"
        return submit_button["value"].split(" ")[0] == "Leave"


    def _get_timeslot_rows(self):
        """
        Get the dict of timeslots and their html row elements
        :return: dict of timeslots: timeslot_row_elements
        :rtype: dict
        """
        response = self._session.get(self._url)
        if response.status_code != 200:
            return False

        # Get the list of available timeslots
        tutorial_soup = BeautifulSoup(response.text, "html.parser")
        timeslot_cells = {timeslot_cell.text: timeslot_cell.parent for timeslot_cell in tutorial_soup.find_all("td", "cell c0")}

        return timeslot_cells


@command()
@argument("username")
@argument("password")
@argument("tutorial_url")
@argument("tutorial_name")
@option("--interval", type=int, default=1)
@option("--timeout", type=int, default=3600)
def main(username, password, tutorial_url, tutorial_name, interval, timeout):
    session = None
    try:
        session = WattleSession(username, password)
    except ConnectionError:
        print("failed to log into wattle")

    tutorial = TutorialListener(session, tutorial_url)
    tutorial.enrol(tutorial_name, interval=interval, timeout=timeout)

if __name__ == '__main__':
    main()